Django==2.2
django-environ==0.4.5
djangorestframework==3.11.0
django-filter==2.2.0
docker-compose==1.25.4
gunicorn==20.0.4
psycopg2-binary==2.8.4
