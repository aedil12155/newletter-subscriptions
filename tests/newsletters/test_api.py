import json

from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from subscriptions.models import Subscription


class TestSubscriptionAPI(TestCase):
    def setUp(self):
        super(TestSubscriptionAPI, self).setUp()

    def test_create_subscription(self):
        url = reverse('subscription-list')

        payload = {
            'email': 'subscribed@email.com'
        }

        resp = self.client.post(url, data=json.dumps(payload), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

        # Try to add same email again should raise 400
        resp = self.client.post(url, data=json.dumps(payload), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_400_BAD_REQUEST)

        # check the state must be subscribed
        subscription = Subscription.objects.get(email=payload['email'])
        self.assertEqual(subscription.state, Subscription.STATE_SUBSCRIBED)

    def test_create_subscription_and_confirm_by_visiting_link(self):
        url = reverse('subscription-list')

        payload = {
            'email': 'subscribed-and-optin@email.com'
        }

        resp = self.client.post(url, data=json.dumps(payload), content_type='application/json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)

        # check the state must be subscribed
        subscription = Subscription.objects.get(email=payload['email'])
        self.assertEqual(subscription.state, Subscription.STATE_SUBSCRIBED)

        opt_in_resp = self.client.get(subscription.confirmation_uri)
        self.assertEqual(opt_in_resp.status_code, status.HTTP_200_OK)

        # check DB if the state is update
        subscription.refresh_from_db()
        self.assertEqual(subscription.state, Subscription.STATE_OPT_IN)
