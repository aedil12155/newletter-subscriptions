FROM python:3.6.4

MAINTAINER Adeel Youans aedil12155@gmail.com

# build dependencies for postgres
RUN set -e; \
    apt-get update -y; \
    apt-get install -y postgresql postgresql-contrib sqlite

ENV         SECRET_KEY      "jasdd@4?)-+QcFyBtsß#i@1ãns@asd@4?+dtgGdvWed=-0dsf4?)-ECrvfYhDgdfgAS#i@1ansd@4?+a"
ENV         PROJECT_NAME    newsletters
ENV         WORKING_DIR     /www/$PROJECT_NAME

WORKDIR $WORKING_DIR

ADD requirements.txt $WORKING_DIR/requirements.txt
RUN pip3 install -r requirements.txt

ADD . $WORKING_DIR

RUN chmod +x ./docker-entrypoint.sh

EXPOSE 8000

CMD ["./docker-entrypoint.sh"]