import random
import string

from django.conf import settings
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.urls import reverse


def generate_random_key(size=32, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))


# content ID, device ID, event type, event time (timestamp)
class Subscription(models.Model):
    STATE_SUBSCRIBED = 'subscribed'
    STATE_OPT_IN = 'opt-in'
    STATE_CHOICES = (
        (STATE_SUBSCRIBED, STATE_SUBSCRIBED.title()),
        (STATE_OPT_IN, STATE_OPT_IN.replace('-', ' ').title())
    )

    email = models.EmailField(null=False, blank=False, unique=True, db_index=True)
    state = models.CharField(max_length=16, default=STATE_SUBSCRIBED, choices=STATE_CHOICES, db_index=True)
    key = models.CharField(max_length=512, null=False, blank=False, default=generate_random_key, db_index=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def opt_in(self):
        if self.state != Subscription.STATE_OPT_IN:
            self.state = Subscription.STATE_OPT_IN
            self.save()

    @property
    def confirmation_uri(self):
        return reverse('subscription-subscription-confirm', kwargs={'key': self.key})

    @property
    def confirmation_link(self):
        return f'{settings.CURRENT_HOST}{self.confirmation_uri}'

    def email_confirmation_link(self):
        send_mail(
            subject='Subscription confirmation link',
            message=f'Click on the link to confirm {self.confirmation_link}',
            from_email='from@example.com',
            recipient_list=['to@example.com'],
            fail_silently=False,
        )

    @staticmethod
    def post_save_actions(sender, instance, created, **kwargs):
        if created:
            instance.email_confirmation_link()


post_save.connect(Subscription.post_save_actions, sender=Subscription)
