from django.conf import settings

from rest_framework import viewsets, permissions, status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from .models import Subscription
from .serializers import SubscriptionSerializer


class SubscriptionViewSet(viewsets.ModelViewSet):
    model = Subscription
    permission_classes = [permissions.AllowAny, ]
    queryset = Subscription.objects.all()
    http_method_names = ['get', 'post', 'put', 'head', 'options']
    serializer_class = SubscriptionSerializer

    def list(self, request, *args, **kwargs):
        if settings.DEBUG:
            return super(SubscriptionViewSet, self).list(request, *args, **kwargs)
        raise NotImplementedError

    def retrieve(self, request, *args, **kwargs):
        if settings.DEBUG:
            return super(SubscriptionViewSet, self).retrieve(request, *args, **kwargs)
        raise NotImplementedError

    @action(detail=False, methods=['GET'], url_path='(?P<key>[^/.]+)/confirm', url_name='subscription-confirm')
    def confirm(self, request, key, **kwargs):
        try:
            subscription = self.get_queryset().get(key=key)
        except Subscription.DoesNotExist:
            raise NotFound()

        subscription.opt_in()

        return Response(status=status.HTTP_200_OK)
