from rest_framework import serializers

from .models import Subscription


class SubscriptionSerializer(serializers.ModelSerializer):
    confirmation_link = serializers.SerializerMethodField()
    class Meta:
        model = Subscription
        fields = ('email', 'confirmation_link')

    def get_confirmation_link(self, obj):
        return obj.confirmation_link