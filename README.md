Table of contents
=================

* [Setting Up](#setting-up)
* [API Documentation](#api-documentation)
* [Running application in Docker](#running-application-in-docker)


## Setting up

Python 3.6 is required for this project, checkout [python website](https://www.python.org/downloads/) on how to setup.

### 1. Setup Environment
You must have [virtualenv](http://www.virtualenv.org/) installed. Once you have virtualenv installed simply create virtualenv for the project.

    $ virtualenv --python=/path/to/python3.6 <virtualenv-name>

After virtualenv is created you can now install project requirements.

    $ source <virtualenv-name>/bin/activate # Activate environment
    $ pip install -r requirements.txt

Run Migrations

    python manage.py migrate

Run Test

    python manage.py test

Once you are done setting up environment and installed requirements you can run application for development purpose as follow,

    python manage.py runserver


**Note:** You must replace required environment variable in `./config/env`, e.g, DATABASE_URL, REDIS_URL etc ... See `config/env.example` for reference

## API Documentation

### Create a new subscription
This will new email in subscription with state `Subscribed`, which means user have just subscribed but haven't confirmed/opt-in (a.k.a double opt-in).

Method: `POST`

URI: `/api/subscriptions/`

Request Data:

    {
        "email": "some@email.com"
    }

Response status code: `201 CREATED`

Response data:

    {
        "email": "some@email.com",
        "confirmation_link": "http://localhost:8000/api/subscriptions/I23WMCO8HOL466499HUI565GOXAXGITM/confirm/"
    }

**Note:** Will also trigger email which can be found in `./emails/` as defined in `settings.EMAIL_FILE_PATH`

### Confirm subscription
By visiting the link provided in the response of the API or send in the email state is update to `Opt In`, which means user have opted in or confirmed

Method: `POST`

URI: `/api/subscriptions/I23WMCO8HOL466499HUI565GOXAXGITM/confirm/`

Response status code: `200 OK`

## Running application in Docker

In order to run whole application along with dependencies in contained environment, one must have docker installed in and also docker-compose.
Once you have docker-compose installed you can simply run `docker-compose up` command that will orchestrate whole environment along with dependencies and
you can access application on URL: `http://127.0.0.1:8000/`

